package com.hsuk.video.consumer.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class VideoViewsCountDto {

    private long videoId;
    private int count;
    private long startTimestamp;
    private long endTimestamp;
}