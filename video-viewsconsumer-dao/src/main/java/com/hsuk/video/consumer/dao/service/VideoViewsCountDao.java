package com.hsuk.video.consumer.dao.service;

import com.hsuk.video.consumer.dto.VideoViewsCountDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

@Slf4j
@Service
public class VideoViewsCountDao {

    private static String APP_IP;

    static {
        try {
            APP_IP = Inet4Address.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            APP_IP = "Unknown";
        }
    }

    private final JdbcTemplate jdbcTemplate;
    private final String query = "INSERT INTO videoviews_count (video_id, views_count, start_period, end_period, created_by, modified_by) VALUES (?, ?, ?, ?, ?, ?)" +
                                    " ON DUPLICATE KEY UPDATE views_count = views_count + ?";

    @Autowired
    public VideoViewsCountDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void saveViewsCount(List<VideoViewsCountDto> viewsCountDto) {
        jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                VideoViewsCountDto dto = viewsCountDto.get(i);
                preparedStatement.setLong(1, dto.getVideoId());
                preparedStatement.setInt(2, dto.getCount());
                preparedStatement.setTimestamp(3, new Timestamp(dto.getStartTimestamp()));
                preparedStatement.setTimestamp(4, new Timestamp(dto.getEndTimestamp()));
                preparedStatement.setString(5, APP_IP);
                preparedStatement.setString(6, APP_IP);
                preparedStatement.setInt(7, dto.getCount());
            }

            @Override
            public int getBatchSize() {
                return viewsCountDto.size();
            }
        });
    }
}